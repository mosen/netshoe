from distutils.version import LooseVersion
import subprocess
import tempfile


class BuildEnvironment(object):

    SIU_RESOURCE_PATHS = {
        '10.13': '/System/Library/PrivateFrameworks/SIUFoundation.framework/XPCServices/com.apple.SIUAgent.xpc/Contents/Resources',
        '10.12': '/System/Library/PrivateFrameworks/SIUFoundation.framework/XPCServices/com.apple.SIUAgent.xpc/Contents/Resources',
        '10.11': '/System/Library/PrivateFrameworks/SIUFoundation.framework/XPCServices/com.apple.SIUAgent.xpc/Contents/Resources',
        '10.10': '/System/Library/CoreServices/System Image Utility.app/Contents/Frameworks/SIUFoundation.framework/Versions/A/XPCServices/com.apple.SIUAgent.xpc/Contents/Resources'
    }

    def __init__(self,
                 version,  # type: str
                 work_dir=None  # type: Optional[str]
                 ):
        """BuildEnvironment represents the host which is building the NetBoot/NetInstall Image.

        :param version: The operating system version, from ``sw_vers -productVersion``.
        """
        self._version = version
        if LooseVersion(self._version) >= '10.13':
            self._siu_resource_path = BuildEnvironment.SIU_RESOURCE_PATHS['10.13']
            self._version_major = '10.13'
        elif LooseVersion(self._version) >= '10.12':
            self._siu_resource_path = BuildEnvironment.SIU_RESOURCE_PATHS['10.12']
            self._version_major = '10.12'
        elif LooseVersion(self._version) >= '10.11':
            self._siu_resource_path = BuildEnvironment.SIU_RESOURCE_PATHS['10.11']
            self._version_major = '10.11'
        elif LooseVersion(self._version) <= '10.10':
            self._siu_resource_path = BuildEnvironment.SIU_RESOURCE_PATHS['10.10']
            self._version_major = '10.10'

        if work_dir is None:
            self._working_directory = tempfile.mkdtemp()
        else:
            self._working_directory = work_dir

    @property
    def siu_resource_path(self):  # type: () -> str
        return self._siu_resource_path

    @property
    def working_directory(self):  # type: () -> str
        """Get the current working directory, where all the intermediate products will be stored."""
        return self._working_directory

    @property
    def version_major(self):  # type: () -> str
        return self._version_major

    @property
    def is_high_sierra(self):  # type: () -> bool
        return self._version_major == '10.13'

    @classmethod
    def from_host(cls):  # type: () -> BuildEnvironment
        """Create a BuildEnvironment instance from the current host.

        :returns: BuildEnvironment
        """
        p = subprocess.Popen(['/usr/bin/sw_vers', '-productVersion'], stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
        version = stdout.strip()

        return cls(version)
