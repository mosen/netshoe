import os.path
import sys
import mimetypes
import plistlib
from hdiutil import Dmg


class InstallSourceError(Exception):
    pass


class InstallSource(object):

    def __init__(self,
                 installer_path  # type: str
                 ):
        """InstallSource describes an installation source.

        :param installer_path: Path to an installer .app bundle.
        """
        self._path = installer_path

    @property
    def path(self):  # type: () -> str
        """
        :return str: The path to the installer app bundle.
        """
        return self._path

    # @property
    # def dmg(self):
    #     return self._dmg

    # @property
    # def dmg_mount_point(self):
    #     mount = None
    #     if len(self.dmg.mount_points) > 1:
    #         for i in self.dmg.mount_points[0]:
    #             if i.find('dmg'):
    #                 mount = i
    #     else:
    #         mount = self.dmg.mount_points[0]
    #
    #     return mount

    @classmethod
    def from_path(cls,
                  installer_path  # type: str
                  ):  # type: () -> InstallSource
        """Generate an instance of InstallSource from the given path.

        In AutoNBI this function is performed differently in 'non-auto' mode.
        This method only covers the auto mode, not the manual picker.

        :param installer_path: The location of a recovery partition or installer app to install
            from.
        :raises IOError: If the installer source is unsuitable or unreadable.
        :returns InstallSource: The install source object.
        :todo: Method is too complex
        """
        if os.path.isdir(installer_path):
            # Remove a potential trailing slash (ie. from autocompletion)
            if installer_path.endswith('/'):
                installer_path = installer_path.rstrip('/')

            if not os.path.exists(installer_path):
                raise InstallSourceError(
                    'The root path {} is not a valid path - unable to proceed.'.format(installer_path))
            elif not installer_path.endswith('.app'):
                print 'Mode is auto but the rootpath is not an installer.app, unable to proceed'
                sys.exit(1)

            elif installer_path.endswith('.app'):
                install_esd_path = os.path.join(installer_path, 'Contents/SharedSupport/InstallESD.dmg')
                if os.path.exists(install_esd_path):
                    return cls(installer_path)
                else:
                    raise InstallSourceError('Unable to locate InstallESD.dmg in {} - exiting.'.format(installer_path))
            else:
                return None  # this should technically never happen, unless you specified a non installer

    def version_information(self):
        """Get information about the operating system version contained within an installer.

        :return dict: Dictionary containing system version information. eg. ProductVersion, ProductBuildVersion
        """
        basesystem = Dmg(os.path.join(self._path, 'Contents', 'SharedSupport', 'BaseSystem.dmg'))
        with basesystem.mounted() as mount_points:
            mount_point = mount_points[0][0]
            systemversion = plistlib.readPlist(os.path.join(mount_point, 'System', 'Library',
                                                            'CoreServices', 'SystemVersion.plist'))

        return systemversion

    # def version_info(self, is_high_sierra=False):
    #     """"getosversioninfo will attempt to retrieve the OS X version and build
    #         from the given mount point by reading /S/L/CS/SystemVersion.plist
    #         Most of the code comes from COSXIP without changes."""
    #
    #     assert self.dmg.mounted
    #
    #     if is_high_sierra:
    #         basesystem_path = os.path.join(self.path, 'Contents/SharedSupport')
    #     else:
    #         basesystem_path = self.dmg_mount_point
    #
    #     # Check for availability of BaseSystem.dmg
    #     basesystem_dmg = os.path.join(basesystem_path, 'BaseSystem.dmg')
    #     if not os.path.isfile(basesystem_dmg):
    #         raise IOError('Missing BaseSystem.dmg in %s' % basesystem_dmg)
    #
    #     # Mount BaseSystem.dmg
    #
    #     basesystem = Dmg(basesystem_dmg)
    #     basesystem.mount()
    #     basesystem_mount_point = basesystem.mount_points[0]
    #
    #     # Read SystemVersion.plist from the mounted BaseSystem.dmg
    #     system_version_plist = os.path.join(
    #         basesystem_mount_point,
    #         'System/Library/CoreServices/SystemVersion.plist')
    #     # Now parse the .plist file
    #     try:
    #         version_info = plistlib.readPlist(system_version_plist)
    #
    #     # Got errors?
    #     except IOError, err:
    #         basesystem.unmount()
    #         raise IOError('Could not read %s: %s' % (system_version_plist, err))
    #
    #     # Done, unmount BaseSystem.dmg
    #     finally:
    #         basesystem.unmount()
    #
    #     # Return the version and build as found in the parsed plist
    #     return version_info.get('ProductUserVisibleVersion'), \
    #            version_info.get('ProductBuildVersion'), basesystem_mount_point
