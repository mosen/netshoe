import os.path
from hdiutil import Dmg


class NBI(object):
    def __init__(self, path):
        """The NBI class represents an already built .nbi bundle.

        It provides convenience methods for modifying the content of the .nbi bundle including the .dmg and NBImageInfo.

        :param path: Path to an existing .nbi bundle.
        """
        self._path = path

    def __str__(self):
        return self._path

    @property
    def netinstall_dmg(self):
        """:returns Dmg: NetInstall dmg instance"""
        netinstall_dmg_path = os.path.join(self._path, 'NetInstall.dmg')
        return Dmg(netinstall_dmg_path)

    def copy(self, src, dst):
        """Copy a source file/directory to a relative destination inside the .nbi's NetInstall.dmg.

        - If the source is a directory, then the entire directory is merged into the NetInstall.dmg at the destination.
        - If the source is a file, then the file is copied to the specified destination.

        :param src: Source file/directory to copy
        :param dst: Destination folder (relative to NetInstall.dmg root).
        """
        pass