import os.path
import subprocess
import shutil
import plistlib
import logging
from typing import Union, Optional, List


class NBIBuilder(object):

    LOG_LEVEL_DEBUG = 'DEBUG'
    LOG_LEVEL_VERBOSE = 'VERBOSE'
    LOG_LEVEL_INFO = 'INFO'

    def __init__(self,
                 buildenv  # type: BuildEnvironment
                 ):
        """NBIBuilder implements functionality common to producing netboot and netinstall environments.

        :param buildenv: BuildEnvironment The current build environment, derived from the host.
        """
        self._buildenv = buildenv  #: BuildEnvironment
        self._source = None
        self._filesystem = 'JHFS+'
        self._volume_name = 'NetBoot'
        self._size_mb = '7000'
        self._log_level = NBIBuilder.LOG_LEVEL_DEBUG

    def log_level(self,
                  level  # type: str
                  ):  # type: (...) -> NBIBuilder
        """
        :param level: Set the logging level returned by the netinstall creation script.
        :return:
        """
        self._log_level = level
        return self


class NetInstallBuilder(NBIBuilder):

    def __init__(self, buildenv):
        super(NetInstallBuilder, self).__init__(buildenv)
        self._packages = []
        self._scripts = []
        self._profiles = []
        self._bsdp_sources = []
        self._enterprise_settings = None
        self._automation = None
        self._fstype = 'JHFS+'

    def source(self,
               source  # type: InstallSource
               ):  # type: (...) -> NetInstallBuilder
        """Specify an installer source."""
        self._source = source
        return self

    def add_packages(self,
                     pkgs  # type: Union[str, List[str]]
                     ):  # type: (...) -> NetInstallBuilder
        """Add package(s) to the NetInstall."""
        if isinstance(pkgs, str):
            assert os.path.exists(pkgs)
            pkgs = [pkgs]
        elif isinstance(pkgs, list):
            for p in pkgs:
                assert os.path.exists(p)

        self._packages.extend(pkgs)
        return self

    def add_scripts(self,
                    scripts  # type: Union[str, List[str]]
                    ):  # type: (...) -> NetInstallBuilder
        """Add script(s) to the NetInstall."""
        if isinstance(scripts, str):
            scripts = [scripts]

        self._scripts.extend(scripts)
        return self

    def add_profiles(self,
                     profiles  # type: Union[str, List[str]]
                     ):  # type: (...) -> NetInstallBuilder
        """Add configuration profile(s) to the NetInstall."""
        if isinstance(profiles, str):
            assert os.path.exists(profiles)
            profiles = [profiles]
        elif isinstance(profiles, list):
            for p in profiles:
                assert os.path.exists(p)

        self._profiles.extend(profiles)
        return self

    def add_bsdp_sources(self,
                         bsdp_sources  # type: Union[str, List[str]]
                         ):  # type: (...) -> NetInstallBuilder
        """Add blessed NetBoot source(s) to the NetInstall."""
        if isinstance(bsdp_sources, str):
            bsdp_sources = [bsdp_sources]

        self._bsdp_sources.extend(bsdp_sources)
        return self

    def enterprise_settings(self,
                            sip_enabled=True,  # type: bool
                            uakel_enabled=True,  # type: bool
                            uakel_team_id_whitelist=None  # type: List[str]
                            ):  # type: (...) -> NetInstallBuilder
        """Set the enterprise settings for SIP and UAKEL.

        :param sip_enabled: System Integrity Protection will be enabled.
        :param uakel_enabled: User Approved Kernel Extension Loading will be enabled.
        :param uakel_team_id_whitelist: List of Team IDs to whitelist for UAKEL.
        """
        self._enterprise_settings = {'SIU-SIP-setting': sip_enabled, 'SIU-SKEL-setting': uakel_enabled}

        if len(uakel_team_id_whitelist) > 0:
            self._enterprise_settings['SIU-teamIDs-to-add'] = uakel_team_id_whitelist

        return self

    def _prep_working_dir(self):
        """Prepare the working directory.

        The createNetinstall.sh script expects createCommon.sh and createVariables.sh to exist in the build environment.
        """
        commonsource = os.path.join(self._buildenv.siu_resource_path, 'createCommon.sh')
        commontarget = os.path.join(self._buildenv.working_directory, 'createCommon.sh')
        shutil.copyfile(commonsource, commontarget)
        open(os.path.join(self._buildenv.working_directory, 'createVariables.sh'), 'a').close()

    def _cleanup_working_dir(self):
        """Remove files that were used temporarily for the build."""
        create_common = os.path.join(self._buildenv.working_directory, 'createCommon.sh')
        create_vars = os.path.join(self._buildenv.working_directory, 'createVariables.sh')


    def _prepare_automation(self):
        """Generate a minstallconfig.xml to be placed into the working directory."""
        if self._automation is not None:
            minstallconfig_path = os.path.join(self._buildenv.working_directory, 'minstallconfig.xml')
            plistlib.writePlist(self._automation, minstallconfig_path)

    def automate(self,
                 target,  # type: str
                 package='/System/Installation/Packages/InstallInfo.plist',  # type: str
                 language='en',  # type: str
                 erase=False  # type: bool
                 ):  # type: (...) -> NetInstallBuilder
        """Automate the NetInstall process by providing a minstallconfig.xml with the given options.

        :param target: The target volume to install to eg. ``/Volumes/Macintosh HD``
        :param package: The package to install automatically.
        :param language: Two letter language code.
        :param erase: Erase the target Disk
        """
        self._automation = {
            'InstallType': 'automated',
            'Language': language,
            'Package': package,
            'ShouldErase': erase,
            'Target': target
        }
        return self

    def __str__(self):
        detail = 'NetShoe Build Details\n'
        detail += '-' * 25 + '\n'
        detail += 'Source Media: {}\n'.format(self._source.path)
        detail += 'Working Directory: {}\n'.format(self._buildenv.working_directory)
        if self._automation:
            detail += 'Automated Installation: YES, to {}\n'.format(self._automation['Target'])
            if self._automation['ShouldErase']:
                detail += 'Automated Erase: YES\n'

        detail += 'Extra Package(s): {}'.format(len(self._packages))

        return detail

    def _prepare_package_list(self):
        """Prepare nominated extra packages list in the format that NetInstall needs."""
        if len(self._packages) > 0:
            with open(os.path.join(self._buildenv.working_directory, 'additionalPackages.txt'), 'w+') as fd:
                fd.writelines(self._packages)

    def _prepare_script_list(self):
        """Prepare nominated extra scripts list in the format that NetInstall needs."""
        if len(self._scripts) > 0:
            with open(os.path.join(self._buildenv.working_directory, 'additionalScripts.txt'), 'w+') as fd:
                fd.writelines(self._scripts)

    def _prepare_profile_list(self):
        """Prepare nominated extra configuration profiles list in the format that NetInstall needs."""
        if len(self._profiles) > 0:
            with open(os.path.join(self._buildenv.working_directory, 'configProfiles.txt'), 'w+') as fd:
                fd.writelines(self._profiles)

    def build(self,
              name  # type: str
              ):  # type: (...) -> str
        """
        Build the NetInstall .nbi

        :param name: The NBI name to produce.
        :return str: Path to the output .nbi bundle
        """
        self._prep_working_dir()

        self._prepare_automation()
        self._prepare_package_list()
        self._prepare_profile_list()
        self._prepare_script_list()

        build_exec = os.path.join(self._buildenv.siu_resource_path, 'createNetInstall.sh')
        command = [build_exec, self._buildenv.working_directory, self._size_mb]
        destination_path = os.path.join(self._buildenv.working_directory, name + '.nbi')

        createvariables = {'destPath': destination_path,
                           'dmgTarget': 'NetInstall',
                           'dmgVolName': name,
                           'destVolFSType': self._fstype,
                           'installSource': self._source.path,
                           'scriptsDebugKey': self._log_level,
                           'ownershipInfoKey': 'root:wheel'}

        proc = subprocess.Popen(command, bufsize=-1, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE, env=createvariables)

        (stdout, stderr) = proc.communicate()
        print(stdout)

        if proc.returncode:
            raise IOError(stderr)

        if not os.path.exists(destination_path):
            raise IOError('Path does not exist: {}'.format(destination_path))

        return destination_path


class NBImageInfoBuilder(object):
    """NBImageInfoBuilder builds the NBImageInfo.plist"""
    def __init__(self):
        self._info = {
            'IsInstall': True,
            'Index': 6667,
            'Name': 'NetBoot Image',
            'Description': 'NetBoot Image',
            'Kind': 1,
            'Language': 'Default',
            'SupportsDiskless': False,
            'RootPath': 'NetInstall.dmg',
            'BootFile': 'booter',
            'Architectures': ['i386'],
            'BackwardCompatible': False,
            'IsEnabled': False,
            'IsDefault': False,
            'Type': 'HTTP',
            'DisabledSystemIdentifiers': [],
            'EnabledSystemIdentifiers': [],
            'imageType': 'netinstall',
            'osVersion': '10.13'
        }

    @classmethod
    def from_platform_support(cls,
                              platform_support_path  # type: str
                              ):  # type: (...) -> NBImageInfoBuilder
        """Generate a new NBImageInfo builder instance, starting with supported system identifiers from a given
        PlatformSupport.plist.

        NOTE: OS X versions prior to 10.11 list both SupportedModelProperties and
            SupportedBoardIds - 10.11 only lists SupportedBoardIds. So we need to
            check both and append to the list if missing. Basically appends any
            model IDs found by looking up their board IDs to 'disabledsystemidentifiers'

        :param platform_support_path: Path to PlatformSupport.plist inside the .nbi eg. ``example.nbi/i386/PlatformSupport.plist``
        :returns: NBImageInfoBuilder seeded with platform support from plist.
        """
        builder = cls()
        platform_support = plistlib.readPlist(platform_support_path)
        builder = builder.disable_identifiers(platform_support['SupportedModelProperties'])

        return builder

    @classmethod
    def from_nbi(cls,
                 nbi_path  # type: str
                 ):  # type: (...) -> NBImageInfoBuilder
        """Generate a new NBImageInfo builder instance, starting with supported system identifiers from an .nbi

        :param nbi_path: Path to an existing NBI.
        :returns: NBImageInfoBuilder seeded with platform support from plist.
        """
        return NBImageInfoBuilder.from_platform_support(os.path.join(nbi_path, 'i386', 'PlatformSupport.plist'))

    def index(self, idx):
        """
        Set the NetBoot image index (default is 6667)

        :param idx: The index number of the netboot image.
        :return: NBImageInfoBuilder
        """
        self._info['Index'] = idx
        return self

    def description(self, desc):
        """
        Set the NetBoot image description (default is 'NetBoot Image')

        :param desc: The description
        :return: NBImageInfoBuilder
        """
        self._info['Description'] = desc
        return self

    def enabled(self, is_enabled=True):
        """
        Enable the NetBoot Image

        :return: NBImageInfoBuilder
        """
        self._info['IsEnabled'] = is_enabled
        return self

    def default(self, is_default=True):
        """
        Make the NetBoot Image the default

        :return: NBImageInfoBuilder
        """
        self._info['IsDefault'] = is_default
        return self

    def enable_identifiers(self, identifiers):
        """
        Enable a list of system identifiers

        :param identifiers: A single identifier or a list of identifiers.
        :return: NBImageInfoBuilder
        """
        if isinstance(identifiers, list):
            self._info['EnabledSystemIdentifiers'] = identifiers
        else:
            self._info['EnabledSystemIdentifiers'] = [identifiers]

        return self

    def disable_identifiers(self, identifiers):
        """
        Disable a list of system identifiers

        :param identifiers: A single identifier or a list of identifiers.
        :return: NBImageInfoBuilder
        """
        if isinstance(identifiers, list):
            self._info['DisabledSystemIdentifiers'] = identifiers
        else:
            self._info['DisabledSystemIdentifiers'] = [identifiers]

        return self

    def build(self):
        """
        Write out a property list and return it.

        :return:
        """
        return plistlib.writePlistToString(self._info)


