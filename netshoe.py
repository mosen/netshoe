#!/usr/bin/python

import os
import argparse
import logging
import shutil

from installer import InstallSource
from environment import BuildEnvironment
from builders import NetInstallBuilder, NBImageInfoBuilder


def main():
    logger = logging.getLogger('netshoe')
    ch = logging.StreamHandler()
    ch.setLevel(logging.WARNING)
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='netshoe')
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='Increase verbosity level')
    parser.add_argument('--source', '-s', required=True,
                        help='Required. Path to Install macOS app')
    parser.add_argument('--output', '-o', default=os.getcwd(),
                        help='Optional. Path to save .plist and .nbi files. Defaults to CWD.')
    parser.add_argument('--auto', '-a', action='store_true', default=False,
                        help='Optional. Toggles automation mode, suitable for scripted runs')
    parser.add_argument('--autoinstall', '-i',
                        help='Automate installation to the given volume (normally /Volumes/Macintosh HD')
    parser.add_argument('--default', '-u', default=True,
                        help='Default netboot image')
    parser.add_argument('--enabled', '-e', default=True,
                        help='Enabled for NetBooting')
    parser.add_argument('--packages', '-k',
                        help='Source additional packages from the given directory')
    parser.add_argument('--scripts', '-r',
                        help='Source additional scripts from the given directory')
    parser.add_argument('--profiles', '-p',
                        help='Source additional configuration profiles from the given directory')
    parser.add_argument('--index', default=5000, dest='nbi_index', type=int,
                        help='Optional. Set a custom Index for the NBI. Default is 5000.')
    parser.add_argument('--lipo', default=False, type=bool,
                        help='Optional. Remove as many items from the system as possible, including the installer.')
    parser.add_argument('--type', default='NFS', dest='nbi_type', choices=['NFS', 'HTTP'],
                        help='Optional. Set a custom Type for the NBI. HTTP or NFS. Default is NFS.')

    # Are we root?
    if os.getuid() != 0:
        logger.error('This tool requires sudo or root privileges.')
        exit(-1)

    arguments = parser.parse_args()

    if arguments.verbose == 2:
        ch.setLevel(logging.DEBUG)
        logger.debug('Log level DEBUG')
    elif arguments.verbose == 1:
        ch.setLevel(logging.INFO)
        logger.info('Log level INFO')

    buildenv = BuildEnvironment.from_host()
    source = InstallSource.from_path(arguments.source)
    builder = NetInstallBuilder(buildenv=buildenv)
    versioninfo = source.version_information()

    logger.info('Building using system version %s (%s)', versioninfo['ProductVersion'], versioninfo['ProductBuildVersion'])

    builder = builder.source(source).log_level(NetInstallBuilder.LOG_LEVEL_DEBUG)
    if arguments.autoinstall:
        logger.debug('Installer will be automated')
        builder = builder.automate(arguments.autoinstall, erase=True)

    # Add packages
    if arguments.packages is not None:
        logger.info('Looking for packages in: %s', arguments.packages)
        for direntry in os.listdir(arguments.packages):
            if not direntry.endswith('pkg'):
                continue

            logger.info('Adding package: %s', os.path.join(arguments.packages, direntry))
            builder.add_packages(os.path.abspath(os.path.join(arguments.packages, direntry)))

    # Add profiles
    if arguments.profiles is not None:
        logger.info('Looking for profiles ending in .mobileConfig or .mobileconfig in: %s', arguments.packages)
        for direntry in os.listdir(arguments.profiles):
            if not direntry.endswith('mobileConfig') or not direntry.endswith('mobileconfig'):
                continue

            logger.info('Adding profile: %s', os.path.join(arguments.profiles, direntry))
            builder.add_profiles(os.path.join(arguments.profiles, direntry))

    print(builder)
    nbi_path = builder.build('netshoe')

    nbinfo = NBImageInfoBuilder.from_nbi(nbi_path)
    nbinfo = nbinfo.default(arguments.default).enabled(arguments.enabled)
    nbinfo = nbinfo.index(arguments.nbi_index)
    nbinfo = nbinfo.description('NetShoe Build')

    with open(os.path.join(nbi_path, 'NBImageInfo.plist'), 'w+') as fd:
        fd.write(nbinfo.build())

    logger.info('Moving %s into output directory: %s', nbi_path, arguments.output)
    shutil.move(nbi_path, os.path.join(arguments.output, os.path.basename(nbi_path)))


if __name__ == '__main__':
    main()
